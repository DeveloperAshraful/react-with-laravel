import React, { Component } from 'react';

class NotFound extends Component {
    render() { 
        return (
            <div className="masthead">
                <div className="container">
                    <div className="intro-text">
                        <h2 className="intro-heading text-uppercase">Page not match</h2>
                    </div>
                </div>
            </div>
        );
    }
}
 
export default NotFound;