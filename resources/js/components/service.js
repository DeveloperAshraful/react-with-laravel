import React, { Component } from 'react';
import Axios from 'axios';

class Service extends Component {

    constructor(){
      super();
      this.state = {
        serviceItems: []
      }
      //console.log(super());
    }

    componentDidMount(){
      axios.get('/api/services').then(response => {
        this.setState({
          serviceItems: response.data
        })
      }).catch(errors => {
        console.log(errors);
      })
    }



    render() { 
        return (
        <section className="page-section" id="services">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 text-center">
                  <h2 className="section-heading text-uppercase">Services</h2>
                  <h3 className="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
              </div>
              <div className="row text-center">
                { 
                  this.state.serviceItems.map((service,i) =>
                    <div className="col-md-4" key={i}>
                      <span className="fa-stack fa-4x">
                        <i className={ service.icon }></i>
                      </span>
                      <h4 className="service-heading">{ service.title }</h4>
                      <p className="text-muted">{ service.description }</p>
                    </div>
                  )
                }
              </div>
            </div>
          </section>
        );
    }
}
 
export default Service;