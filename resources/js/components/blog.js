import React, { Component } from 'react';

import axios from 'axios';

class Blog extends Component {
    
    constructor(){
        super();
        this.state = {
            blogitems: []
        }
        //console.log(super());
    }
    componentDidMount(){
        axios.get('/api/blogs').then(response => {
            this.setState({
                blogitems: response.data
            })
        }).catch(errors => {
            console.log(errors);
        })
    }

    render() {
        return (
            <div className="container">
                <br /><br />
                <div className="row justify-content-center">
                    <div className="col-md-8">           
                        {
                            this.state.blogitems.map((blog,i) =>
                                <div className="card mb-4" key={i}>
                                    <div className="card-body">
                                        <h2>{ blog.title }</h2>
                                        <p>{ blog.content }</p>
                                    </div>
                                </div>
                            )
                        }
                        
                    </div>
                </div>
            </div>
        );
    }
}
 
export default Blog;