import React, { Component, Fragment } from 'react';
import Header from '../components/header';
import Blog from '../components/blog';
import Footer from '../components/footer';

class BlogPage extends Component {
    render() { 
        return (
            <Fragment>
                <Header />
                <Blog />
                <Footer />
            </Fragment>
        );
    }
}
 
export default BlogPage;