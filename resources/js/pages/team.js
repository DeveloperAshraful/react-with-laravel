import React, { Component, Fragment } from 'react';
import Header from '../components/header';
import Footer from '../components/footer';
import Team from '../components/team';

class TeamPage extends Component {
    render() { 
        return (
            <Fragment>
                <Header />
                <Team />
                <Footer />
            </Fragment>
        );
    }
}
 
export default TeamPage;