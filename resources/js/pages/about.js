import React, { Component, Fragment } from 'react';
import Header from '../components/header';
import Footer from '../components/footer';
import Banner from '../components/banner';

class AboutPage extends Component {
    render() { 
        return (
            <Fragment>
                <Header />
                <Banner />
                <Footer />
            </Fragment>
        );
    }
}
 
export default AboutPage;