import React, { Component, Fragment } from 'react';
import Header from '../components/header';
import Footer from '../components/footer';
import Banner from '../components/banner';
import Service from '../components/service';
import Team from '../components/team';

class HomePage extends Component {
    render() { 
        return (
            <Fragment>
                <Header />
                <Banner />
                <Service />
                <Team />           
                <Footer />
            </Fragment>
        );
    }
}
 
export default HomePage;