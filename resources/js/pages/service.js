import React, { Component, Fragment } from 'react';
import Header from '../components/header';
import Footer from '../components/footer';
import Service from '../components/service';

class ServicePage extends Component {
    render() { 
        return (
            <Fragment>
                <Header />
                <Service />
                <Footer />
            </Fragment>
        );
    }
}
 
export default ServicePage;