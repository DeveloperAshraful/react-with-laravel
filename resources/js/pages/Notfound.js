import React, {Component, Fragment} from 'react';
import Header from '../components/header';
import NotFound from '../components/NotFound';
import Footer from '../components/footer';

class NotoundPage extends Component {
    render(){ 
        return (
            <Fragment>
                <Header />
                <NotFound />
                <Footer />
            </Fragment>
        );
    }
}
 
export default NotoundPage;