import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import Routerlist from './routelist';

function Index() {
    return (
        <Fragment>
            <BrowserRouter>
                <Routerlist />
            </BrowserRouter>
        </Fragment>
    );
}

export default Index;

if (document.getElementById('root')) {
    ReactDOM.render(<Index />, document.getElementById('root'));
}