import React, { Component, Fragment } from 'react'
import {Switch,Route,Link} from "react-router-dom";

import HomePage from './pages/home';
import AboutPage from './pages/about';
import BlogPage from './pages/blog';
import ServicePage from './pages/service';
import NotoundPage from './pages/Notfound';
import TeamPage from './pages/team';

class Routerlist extends Component {

  render () {
    return (
        <Fragment>
            <Switch>
              <Route exact path="/" component={HomePage}></Route>
              <Route exact path="/about" component={AboutPage}></Route>
              <Route exact path="/blog" component={BlogPage}></Route>
              <Route exact path="/service" component={ServicePage}></Route>
              <Route exact path="/team" component={TeamPage}></Route>
              <Route exact component={NotoundPage}></Route>
            </Switch>
        </Fragment>
    )
  }
}

export default Routerlist;