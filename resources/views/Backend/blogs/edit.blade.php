@extends('Backend.partials.layout')
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card shadow mb-4">
                <!-- Card Header -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit Blog</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('blog.update', $blog->id) }}" method="post" class="forms-sample" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Blog Title</label>
                            <input type="text" name="name" value="{{ $blog->title }}" class="form-control" placeholder="Blog Title">
                        </div>
                        <div class="form-group">
                            <label for="description">Blog description</label>
                            <textarea class="form-control" name="description" placeholder="Blog description" rows="3">{{ $blog->content }}</textarea>
                        </div>                
                        <button type="submit" class="btn btn-success mr-2">Update Blog</button>
                    </form>
              </div>
            </div>
        </div>
    </div>

@endsection

    

