@extends('Backend.partials.layout')

@section('content')


		<div class="col-lg-12">
			<div class="d-sm-flex align-items-center justify-content-between mb-4">
				<a href="{{ route('blog.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add New Blog</a>
			</div>
		</div>

		<div class="col-lg-12 grid-margin stretch-card">
			<div class="card shadow mb-4">
				<!-- Card Header -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Blog List</h6>
                </div>
				<div class="card-body">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th> Number </th>
								<th> Title </th>
								<th> Content </th>
								<th> Action </th>
							</tr>
						</thead>
						<tbody>

						@php $i=1; @endphp
						@foreach($blogs as $blog)	
						
							<tr>
								<td> {{ $i }} </td>
								<td>{{ $blog->title }}</td>
								<td>{{ Str::words($blog->content, 10,'...') }}</td>
								<td class="form-inline">
									<a href="{{ route('blog.edit',$blog->id ) }}" class="btn btn-primary btn-icon-split"><span class="icon text-white-50">
									<i class="fas fa-edit"></i></span><span class="text">Edit</span></a>
									
									<a class="btn btn-danger ml-2" href="#" data-toggle="modal" data-target="#deleteModal{{ $blog->id }}">
										<span class="icon text-white-50"><i class="fas fa-trash"></i></span>
										<span class="text">Delete</span>
									</a>

									<!-- Delete Modal-->
									<div class="modal fade" id="deleteModal{{ $blog->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
												<button class="close" type="button" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">×</span>
												</button>
												</div>
												<form action="{{ route('blog.delete', $blog->id) }}" method="post" class="forms-sample" enctype="multipart/form-data">
													@csrf
													<div class="modal-body">This Blog Will be deleted Permanently.</div>
													<div class="modal-footer">
													<button class="btn btn-danger" type="submit">Confirm</button>
												</div>
											</form>
											</div>
										</div>
									</div>
							    	

								</td>
							</tr>
						@php $i++; @endphp
						@endforeach

					
							
						</tbody>
					</table>
			</div>
		</div>

@endsection