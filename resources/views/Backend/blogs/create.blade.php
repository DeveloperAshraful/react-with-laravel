@extends('Backend.partials.layout')
@section('content')
  <div class="row">
      <div class="col-md-12 grid-margin stretch-card">
            <div class="card shadow mb-4">
                <!-- Card Header -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add Blog</h6>
                </div>
                <div class="card-body">
                  <form action="{{ route('blog.create') }}" method="post" class="forms-sample" enctype="multipart/form-data">
                    {{ csrf_field() }}
                      <div class="form-group">
                          <label for="name">Blog Title</label>
                          <input type="text" name="name" class="form-control" placeholder="Blog Title">
                      </div>
                      <div class="form-group">
                          <label for="description">Blog content</label>
                          <textarea class="form-control" name="description" placeholder="Blog description" rows="3"></textarea>
                      </div>               
                      <button type="submit" class="btn btn-success mr-2">Add Blog</button>
                  </form>
              </div>
          </div>
      </div>
  </div>

@endsection


    

