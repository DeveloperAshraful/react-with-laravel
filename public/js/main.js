
(function($) {
    'use strict';


    // Product CAROUSEL
    
    $('.product-carousel').owlCarousel({
        loop:true,
        navText:['<i class="fal fa-chevron-left"></i>','<i class="fal fa-chevron-right"></i>'],
        nav:true,
        autoplay:true,
        margin: 20,
        smartSpeed:1000,
        autoplayHoverPause:true,
        responsive:{
            0:{
            items:1
            },
            600:{
                items:2
            },
            1000:{
                 items:4
             }
         }
     });

    // Products thumbs

    $(".preview a").on("click", function() {
        $(".selected").removeClass("selected");
        $(this).addClass("selected");
        var picture = $(this).data();

        event.preventDefault(); //prevents page from reloading every time you click a thumbnail

        $(".full img").fadeOut(100, function() {
            $(".full img").attr("src", picture.full);
            $(".full").attr("href", picture.full);
            $(".full").attr("title", picture.title);

        }).fadeIn();

    }); 

    // payment method change

    $("#payments").change(function() {
        let payment_method = $("#payments").val();
        
        if(payment_method == "cashin") {
            $("#payment_cashin").addClass("show");
            $("#payment_bkash").removeClass("show");
            $("#payment_rocket").removeClass("show");
            $("#transaction").hide();
        }else if(payment_method == "bkash"){
            $("#payment_bkash").addClass("show");
            $("#payment_cashin").removeClass("show");
            $("#payment_rocket").removeClass("show");
            $("#transaction").show();
        }else if(payment_method == "rocket"){
            $("#payment_rocket").addClass("show");
            $("#payment_cashin").removeClass("show");
            $("#payment_bkash").removeClass("show");
            $("#transaction").show();
        }else if(payment_method == ""){
            $("#payment_cashin").removeClass("show");
            $("#payment_bkash").removeClass("show");
            $("#payment_rocket").removeClass("show");
            $("#transaction").hide();
        }

        
    }); 

    // Products zoom
        
    $('.picZoomer').picZoomer();
    $('.thumbnail li').on('click',function(event){
        var $pic = $(this).find('img');
        $('.picZoomer-pic').attr('src',$pic.attr('src'));
    }); 







	
})(jQuery);



