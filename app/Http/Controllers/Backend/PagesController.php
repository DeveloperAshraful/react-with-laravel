<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Blog;
use App\Models\Service;


class PagesController extends Controller
{
    
    public function index(){
        return view('Backend.pages.index');
    }


}
