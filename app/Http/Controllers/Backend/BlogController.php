<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use App\Models\Blog;


class BlogController extends Controller
{
    
    public function index(){
        $blogs = Blog::orderby('id','desc')->get();
        return view('Backend.blogs.index', compact('blogs'));
    }
    public function create(){
        return view('Backend.blogs.create');
    }
    public function store( Request $request){
        $blog = new Blog;
		$blog->title = $request->name;
        $blog->slug = Str::slug($request->name);
        //$blog->slug = $slug->createSlug($request->name);
        $blog->content = $request->description;
        $blog->save();
    	return redirect()->route('blog.index');
    }
    public function edit($id){
        $blog = Blog::find($id);
        return view('Backend.blogs.edit', compact('blog'));
    }
    public function update( Request $request, $id){
        $blog = Blog::find($id);
		$blog->title = $request->name;
        $blog->slug = Str::slug($request->name);
        //$blog->slug = $slug->createSlug($request->name);
        $blog->content = $request->description;
        $blog->save();
    	return redirect()->route('blog.index');
    }

    public function delete($id){
        $blog = Blog::find($id);
        $blog->delete();
        return redirect()->route('blog.index');
    }



}
