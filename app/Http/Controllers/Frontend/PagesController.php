<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Blog;
use App\Models\Service;

class PagesController extends Controller
{
    public function blog(){
        $blogs = Blog::all();
        return response()->json($blogs);
    }
    public function service(){
        $services = Service::all();
        return response()->json($services);
    }
 





}
