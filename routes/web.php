<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function(){
    Route::get('/', 'Backend\PagesController@index')->name('index');
    // Blog Route
    Route::group(['prefix' => 'blog'], function(){
        Route::get('/', 'Backend\BlogController@index')->name('blog.index');
        Route::get('/create', 'Backend\BlogController@create')->name('blog.create');
        Route::post('/create', 'Backend\BlogController@store')->name('blog.store');
        Route::get('/edit/{id}', 'Backend\BlogController@edit')->name('blog.edit');
        Route::post('/update/{id}', 'Backend\BlogController@update')->name('blog.update');
        Route::post('/delete/{id}', 'Backend\BlogController@delete')->name('blog.delete');
    });
});

// Route::get('/blogs', function () {
//     $blogArticles = DB::table('blogs')->get();
//     return view('frontend/index');
// });

